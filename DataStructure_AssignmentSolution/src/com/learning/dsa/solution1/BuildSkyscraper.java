/**
 * 
 */
package com.learning.dsa.solution1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

/**
 * @author Tarang Gupta
 *
 */
public class BuildSkyscraper {

	// to store the constructed floors of different size 
	private Stack<Integer> constructedFloor;

	// to store the Skyscraper construction order 
	private Queue<List<Integer>> constructionOrder;

	// to store the building height
	private int buildingHeight;

	public BuildSkyscraper(int buildingHeight) {
		constructedFloor = new Stack<Integer>();
		constructionOrder = new LinkedList<>();
		this.buildingHeight = buildingHeight;
	}

	/**
	 * To create floor construction order
	 * 
	 * @param floor 
	 * size of floor constructed on particular day
	 * 
	 * @return void
	 */
	public void constructSkyscraper(int floor) {
		
		// to store the floor construction order in a day
		List<Integer> floorOrder = new ArrayList<>();

		constructedFloor.push(floor);

		if (floor == buildingHeight) {

			int height = buildingHeight;

			while (!constructedFloor.isEmpty() && height == constructedFloor.peek()) {
				floorOrder.add(constructedFloor.pop());
				height--;
			}
			
			constructionOrder.add(floorOrder);
			buildingHeight = buildingHeight - floorOrder.size();
		} else {
			constructionOrder.add(new ArrayList<>());
		}

	}
	
	/**
	 * To print floor construction order
	 * 
	 * @return void
	 */
	public void printOrderConstruction() {

		int i = 0;
		for (List<Integer> list : constructionOrder) {

			System.out.print("\nDay : " + ++i);
			System.out.println();
			for (int n : list) {
				System.out.print(n + " ");
			}

		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner sc = new Scanner(System.in);

		System.out.println("\n Enter the total no of floors in the building ");
		int buildingHeight = sc.nextInt();
		BuildSkyscraper build = new BuildSkyscraper(buildingHeight);

		for (int i = 0; i < buildingHeight; i++) {
			System.out.print("\nenter the floor size given on day :" + (i + 1));
			int floor = sc.nextInt();

			build.constructSkyscraper(floor);
		}

		System.out.println("The order of construction is as follows: ");

		build.printOrderConstruction();
		sc.close();
	}

}
