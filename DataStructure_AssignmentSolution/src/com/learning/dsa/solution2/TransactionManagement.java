package com.learning.dsa.solution2;

class Node {
	int val;
	Node left, right;

	public Node(int val) {
		this.val = val;
		left = right = null;
	}
}

public class TransactionManagement {

	public Node node;
	Node prevNode = null;
	Node headNode = null;

	
	/**
	 * To change the transaction management according to new requirements. 
	 * It will convert BST to right skewed BST
	 * 
	 * @param root 
	 * root of the BST
	 * 
	 * @return void
	 */
	public void changeStructure(Node root) {

		// Base Case
		if (root == null) {
			return;
		}

		changeStructure(root.left);

		Node rightNode = root.right;

		// Condition to check if the root Node
		// of the skewed tree is not defined
		if (headNode == null) {
			headNode = root;
			root.left = null;
			prevNode = root;
		} else {
			prevNode.right = root;
			root.left = null;
			prevNode = root;
		}

		changeStructure(rightNode);

	}
	
	/**
	 * To print transactions in increasing order
	 * inorder Traversal
	 * 
	 * @param root 
	 * root of the BST
	 * 
	 * @return void
	 */
	public void display(Node root) {

		if (root == null) {
			return;
		}
		
		System.out.print(root.val + " ");
		display(root.right);

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TransactionManagement tree = new TransactionManagement();
		tree.node = new Node(50);
		tree.node.left = new Node(30);
		tree.node.right = new Node(60);
		tree.node.left.left = new Node(10);
		tree.node.right.left = new Node(55);

		tree.changeStructure(tree.node);
		tree.display(tree.headNode);

	}

}
